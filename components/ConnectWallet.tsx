export default function ConnectWallet({
  handleConnectWallet,
}: {
  handleConnectWallet: any;
}) {
  return (
    <div className="grid place-items-center h-screen w-screen bg-gray-900">
      <div className="">
        <div className="mx-auto max-w-2xl text-center">
          <h2 className="text-3xl font-bold tracking-tight text-white sm:text-4xl">
            Boost your privacy.
            <br />
            Start using our app today.
          </h2>
          <p className="mx-auto mt-6 max-w-xl text-lg leading-8 text-gray-300">
            Secured by blockchain
          </p>
          <div className="mt-10 flex items-center justify-center gap-x-6">
            <button
              className="bg-white rounded-xl py-2 px-20 font-semibold"
              onClick={handleConnectWallet}
            >
              Connect Wallet
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
