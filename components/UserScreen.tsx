import React from "react";
import { PlusIcon, TrashIcon } from "@heroicons/react/24/outline";

function UserScreen({
  addTask,
  tasks,
  setInput,
  input,
}: {
  addTask: any;
  tasks: any;
  setInput: any;
  input: any;
}) {
  return (
    <div className="w-screen h-screen grid place-items-center bg-black">
      <div className="w-[80vw] h-[80vh] p-20 bg-white bg-opacity-10 backdrop-blur-[0.6px] rounded-2xl">
        <div className="">
          <h1 className="text-3xl font-semibold text-white">Medical Records</h1>
          <div className="space-y-2 overflow-y-auto mt-4">
            <div className="flex items-center ">
              <input
                onChange={(e) => setInput(e.target.value)}
                value={input}
                className="flex-1 text-xl text-white border border-white rounded-lg bg-black bg-opacity-50 focus:ring-black focus:outline-none focus:shadow-none rounded-lg p-2"
              />
              <button
                className="p-1 rounded-full bg-pink-600 ml-4"
                onClick={addTask}
              >
                <PlusIcon className="w-6 h-6 text-white font-bold " />
              </button>
            </div>
            <div className="h-72 overflow-y-scroll scrollbar-hidden">
              {tasks.map((el: any) => (
                <div className="flex items-center border border-white rounded-lg">
                  <p className="text-xl text-white bg-black bg-opacity-50 w-full rounded-lg p-2">
                    {el.taskText}
                  </p>
                  {/* <button className="p-1 rounded-full bg-blue-600 ml-4">
                  <TrashIcon className="w-5 h-5 text-white font-bold " />
                </button> */}
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default UserScreen;
