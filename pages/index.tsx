import { useEffect, useState } from "react";
import TaskAbi from "../contracts/_Todolist_abi.json";
import { TaskContractAddress } from "../contracts/config";
import ConnectWallet from "../components/ConnectWallet";
import UserScreen from "../components/UserScreen";
import { ethers } from "ethers";

export default function Home() {
  const [isUserLoggedIn, setIsUserLoggedIn] = useState<boolean>();
  const [currentAccount, setCurrentAccount] = useState<any>();
  const [input, setInput] = useState<any>();
  const [tasks, setTasks] = useState<any>([]);

  useEffect(() => {
    getAllTask();
  }, []);

  const handleConnectWallet = async () => {
    try {
      const { ethereum }: any = window;
      if (!ethereum) {
        console.log("wallet not found");
        return;
      }

      let chainId = await ethereum.request({ method: "eth_chainId" });

      const accounts = await ethereum.request({
        method: "eth_requestAccounts",
      });

      console.log("found account");
      setIsUserLoggedIn(true);
      setCurrentAccount(accounts[0]);
    } catch (error) {}
  };

  const getAllTask = async () => {
    try {
      const { ethereum }: any = window;
      if (!ethereum) {
        console.log("wallet not found");
        return;
      }

      const provider = new ethers.providers.Web3Provider(ethereum);
      const signer = provider.getSigner();
      const TaskContract = new ethers.Contract(
        TaskContractAddress,
        TaskAbi,
        signer
      );

      let allTasks = await TaskContract.getMyTasks();
      console.log(allTasks);
      setTasks(allTasks);
    } catch (error) {}
  };

  const addTask = async (e: any) => {
    if (!input) return;
    let task = {
      taskText: input,
      isDeleted: false,
    };
    try {
      const { ethereum }: any = window;
      if (!ethereum) {
        console.log("wallet not found");
        return;
      }

      const provider = new ethers.providers.Web3Provider(ethereum);
      const signer = provider.getSigner();

      const TaskContract = new ethers.Contract(
        TaskContractAddress,
        TaskAbi,
        signer
      );
      TaskContract.addTask(task.taskText, task.isDeleted).then(() => {
        setInput("");
        setTasks([...tasks, task]);
      });
    } catch (error) {}
  };

  return (
    <div className="font-satoshi">
      {isUserLoggedIn ? (
        <UserScreen
          addTask={addTask}
          tasks={tasks}
          setInput={setInput}
          input={input}
        />
      ) : (
        <ConnectWallet handleConnectWallet={handleConnectWallet} />
      )}
    </div>
  );
}
